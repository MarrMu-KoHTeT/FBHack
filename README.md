# FBHack (Dark FB)


# Requirements 
<br> [Python2](https://www.python.org/download/releases/2.0/)
<br> [python2-pip](https://docs.python.org/2/installing/)
<br> mechanize
<br> requests
<br> [git](https://git-scm.com/downloads)


# How to use :
# 1. LOGIN ON OPERA MINI
<br> 2. git clone https://gitlab.com/ferdikennedy/FBHack
<br> 3. cd FBHack
<br> 4. Install requirements
<br> 5. python2 love.py


>  Hacking is not crime.

Any question? Just DM me on [Instagram](https://www.instagram.com/frdy_an)